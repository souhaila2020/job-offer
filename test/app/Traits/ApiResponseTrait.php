<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait ApiResponseTrait
{
    /**
     * Respond with a success JSON response.
     *
     * @param mixed $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successResponse($data, $statusCode = Response::HTTP_OK): JsonResponse
    {
        return response()->json(['data' => $data], $statusCode);
    }

    /**
     * Respond with an error JSON response.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse($message, $statusCode): JsonResponse
    {
        return response()->json(['error' => $message], $statusCode);
    }
}

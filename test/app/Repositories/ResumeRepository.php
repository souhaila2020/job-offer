<?php
namespace App\Repositories;

use App\Models\Resume;

class ResumeRepository
{
    public function create(array $data)
    {
        return Resume::create($data);
    }

    public function update(Resume $resume, array $data)
    {
        $resume->update($data);
        return $resume;
    }

    public function delete(Resume $resume)
    {
        $resume->delete();
    }

    public function getById($id)
    {
        return Resume::findOrFail($id);
    }

    public function getAll()
    {
        return Resume::all();
    }
}

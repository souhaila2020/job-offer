<?php
namespace App\Repositories;

use App\Models\JobOffer;

class JobOfferRepository
{
    /**
     * Get all job offers.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return JobOffer::all();
    }

    /**
     * Find a job offer by ID.
     *
     * @param int $id
     * @return \App\Models\JobOffer|null
     */
    public function find($id)
    {
        return JobOffer::findOrFail($id);
    }

    /**
     * Create a new job offer.
     *
     * @param array $data
     * @return \App\Models\JobOffer
     */
    public function create(array $data)
    {
        return JobOffer::create($data);
    }

    /**
     * Update a job offer.
     *
     * @param \App\Models\JobOffer $jobOffer
     * @param array $data
     * @return \App\Models\JobOffer
     */
    public function update(JobOffer $jobOffer, array $data)
    {
        $jobOffer->update($data);
        return $jobOffer;
    }

    /**
     * Delete a job offer.
     *
     * @param \App\Models\JobOffer $jobOffer
     * @return void
     */
    public function delete(JobOffer $jobOffer)
    {
        $jobOffer->delete();
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email|unique:users,email,' . $this->route('user'), // Ignore the current user's email during uniqueness check
            'password' => 'sometimes|string|min:8|confirmed', // Only validate password if it's present and confirmed
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateResumeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'formation' => 'required|string',
            'experience' => 'required|string',
            'competence' => 'required|string',
            
        ];
    }
}

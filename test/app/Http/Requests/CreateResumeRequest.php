<?php
// CreateResumeRequest.php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateResumeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'formation' => 'required|string|max:255',
            'experience' => 'required|string|max:255',
            'competence' => 'required|string|max:255',
            // 'pdf_path' => 'required|string',
        ];
    }
}


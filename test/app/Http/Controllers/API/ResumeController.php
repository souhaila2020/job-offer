<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\CreateResumeRequest;
use App\Http\Requests\UpdateResumeRequest;
use App\Http\Resources\ResumeResource;
use App\Repositories\ResumeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Traits\ApiResponseTrait;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;


class ResumeController extends Controller
{

    use ApiResponseTrait;
    private $resumeRepository;

    public function __construct(ResumeRepository $resumeRepository)
    {
        $this->resumeRepository = $resumeRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/resumes",
     *     summary="Get all resumes",
     *     tags={"resumes"},
     *     @OA\Response(
     *         response=200,
     *         description="List of resumes",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function index()
    {
        try {
            $resumes = $this->resumeRepository->getAll();
            return ResumeResource::collection($resumes);
        } catch (\Exception $e) {
            return $this->errorResponse('Error retrieving resumes', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/resumes/{id}",
     *     summary="Get a specific resume by ID",
     *     tags={"resumes"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the resume",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Resume details",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Resume not found",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function show($id)
    {
        try {
            $resume = $this->resumeRepository->getById($id);

            if (!$resume) {
                return $this->errorResponse('Resume not found', Response::HTTP_NOT_FOUND);
            }

            return new ResumeResource($resume);
        } catch (\Exception $e) {
            return $this->errorResponse('Error retrieving resume', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/resumes",
     *     summary="Create a new resume",
     *     tags={"resumes"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="formation", type="string", example="Education details"),
     *             @OA\Property(property="experience", type="string", example="Work experience"),
     *             @OA\Property(property="competence", type="string", example="Skills"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Resume created successfully",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function store(CreateResumeRequest $request)
    {
        try {
            $data = $request->validated();
            // Log the entire content of the request
            Log::info('Request Content:', ['content' => request()->all()]);

            // Generate PDF
            $pdfContent = $this->generatePdf($data);

            // Store PDF in the database
            $resume = $this->resumeRepository->create(['pdf_path' => $pdfContent] + $data);

            // Log successful resume creation
            Log::info('Resume created successfully.');

            // Return the response with the PDF link
            return response()->json([
                'message' => 'Resume created successfully',
                'data' => new ResumeResource($resume),
                'pdf_path' => $resume->pdf_path,
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            // Log the exception for debugging
            Log::error('Error creating resume: ' . $e->getMessage());

            return response()->json([
                'error' => 'Error creating resume',
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }



    /**
     * @OA\Put(
     *     path="/api/resumes/{id}",
     *     summary="Update a resume",
     *     tags={"resumes"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the resume",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="formation", type="string", example="Education details"),
     *             @OA\Property(property="experience", type="string", example="Work experience"),
     *             @OA\Property(property="competence", type="string", example="Skills"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Resume updated successfully",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Resume not found",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function update(UpdateResumeRequest $request, $id)
    {
        try {
            $resume = $this->resumeRepository->getById($id);

            if (!$resume) {
                return response()->json(['error' => 'Resume not found'], Response::HTTP_NOT_FOUND);
            }

            $data = $request->validated();

            // Generate PDF
            $pdfContent = $this->generatePdf($data);

            // Update PDF in the database
            $resume = $this->resumeRepository->update($resume, ['pdf_path' => $pdfContent] + $data);

            // Return the response with the PDF link
            return response()->json([
                'message' => 'Resume updated successfully',
                'data' => new ResumeResource($resume),
                'pdf_path' => $resume->pdf_path,
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Error updating resume',
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * @OA\Delete(
     *     path="/api/resumes/{id}",
     *     summary="Delete a resume",
     *     tags={"resumes"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the resume",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Resume deleted successfully"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Resume not found",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function destroy($id)
    {
        try {
            $resume = $this->resumeRepository->getById($id);

            if (!$resume) {
                return $this->errorResponse('Resume not found', Response::HTTP_NOT_FOUND);
            }

            $this->resumeRepository->delete($resume);

            return response()->json(null, Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return $this->errorResponse('Error deleting resume', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }



    private function generatePdf($data)
    {
        $temporaryStorage = 'temp_pdfs';

        if (!File::isDirectory($temporaryStorage)) {
            File::makeDirectory($temporaryStorage, 0755, true);
        }

        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isPhpEnabled', true);

        $dompdf = new Dompdf($options);

        $html = view('pdf_template', compact('data'))->render();

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $pdfPath = $temporaryStorage . '/' . uniqid('resume_') . '.pdf';
        File::put($pdfPath, $dompdf->output());

        return $pdfPath;
    }
}




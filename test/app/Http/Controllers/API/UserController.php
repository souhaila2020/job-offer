<?php

namespace App\Http\Controllers\API;

use App\Events\JobApplicationEvent;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\JobOffer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/users",
     *     summary="Get all users",
     *     tags={"Users"},
     *     @OA\Response(
     *         response=200,
     *         description="List of users",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function index()
    {
        try {
            $users = User::all();

            return UserResource::collection($users);
        } catch (\Exception $e) {
            return $this->errorResponse('Error retrieving users', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/users/{id}",
     *     summary="Get a specific user by ID",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the user",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User details",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User not found",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function show($id)
    {
        try {
            $user = User::find($id);

            if (!$user) {
                return $this->errorResponse('User not found', Response::HTTP_NOT_FOUND);
            }

            return new UserResource($user);
        } catch (\Exception $e) {
            return $this->errorResponse('Error retrieving user', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/users/{id}",
     *     summary="Update a user",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the user",
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="name", type="string", example="John Doe"),
     *             @OA\Property(property="email", type="string", example="john@example.com"),
     *             @OA\Property(property="password", type="string", example="newpassword123"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User updated successfully",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User not found",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $user = User::find($id);

            if (!$user) {
                return $this->errorResponse('User not found', Response::HTTP_NOT_FOUND);
            }

            $data = $request->validated();
            $user->update($data);

            return new UserResource($user);
        } catch (\Exception $e) {
            return $this->errorResponse('Error updating user', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/users/{id}",
     *     summary="Delete a user",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the user",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="User deleted successfully",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User not found",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);

            if (!$user) {
                return $this->errorResponse('User not found', Response::HTTP_NOT_FOUND);
            }

            $user->delete();

            return response()->json([], Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return $this->errorResponse('Error deleting user', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    public function applyForJob(Request $request, $jobOfferId)
    {
        try {
            // Log the entire content of the request
            Log::info('Request Content:', ['content' => request()->all()]);
            Log::info('Request jobOfferId:', ['jobOfferId' => $jobOfferId]);
            // Find the job offer
            $jobOffer = JobOffer::find($jobOfferId);
            if (!$jobOffer) {
                return response()->json(['message' => 'Job offer not found.'], 404);
            }

            // Validate the request data
            // $request->validate([
            //     'resume' => 'required|file|mimes:pdf|max:2048',
            // ]);

            // Get the resume file from the request
            // $resumeFile = $request->file('resume');
            // Debugging: Log the contents of $resumeFile
            // Log::info('Resume File Info:', ['file' => $resumeFile]);

            // // Save the resume file
            // $resumePath = $resumeFile->store('resumes', 'public');

            // Dispatch the JobApplicationEvent
            // event(new JobApplicationEvent($jobOffer, $resumePath));
            Log::info('JobApplicationEvent triggered');
            event(new JobApplicationEvent($jobOffer));

            return response()->json(['message' => 'Application submitted successfully']);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json(['message' => 'Validation error', 'errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['message' => 'An error occurred while applying for the job.'], 500);
        }
    }




}

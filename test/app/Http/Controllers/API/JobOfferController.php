<?php

namespace App\Http\Controllers\API;

use App\Repositories\JobOfferRepository;
use App\Http\Requests\CreateJobOfferRequest;
use App\Http\Requests\UpdateJobOfferRequest;
use App\Http\Resources\JobOfferResource;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;


/**
 * @OA\Info(
 *      title="Your API",
 *      version="1.0.0",
 *      description="Your API description",
 *      @OA\License(
 *          name="Your License",
 *          url="http://127.0.0.1:8000"
 *      )
 * )
 * @OA\Tag(
 *     name="Job Offers",
 *     description="API Endpoints for managing job offers"
 * )
 */

class JobOfferController extends Controller
{
    use ApiResponseTrait;

    private $jobOfferRepository;

    public function __construct(JobOfferRepository $jobOfferRepository)
    {
        $this->jobOfferRepository = $jobOfferRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/job-offers",
     *     summary="Get all job offers",
     *     tags={"jobOffers"},
     *     @OA\Response(
     *         response=200,
     *         description="List of job offers",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function index()
    {
        try {
            $jobOffers = $this->jobOfferRepository->all();
            return JobOfferResource::collection($jobOffers);
        } catch (\Exception $e) {
            return $this->errorResponse('Error retrieving job offers', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/job-offers/{id}",
     *     summary="Get a specific job offer by ID",
     *     tags={"jobOffers"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the job offer",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Job offer details",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Job offer not found",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function show($id)
    {
        try {
            $jobOffer = $this->jobOfferRepository->find($id);

            if (!$jobOffer) {
                return $this->errorResponse('Job offer not found', Response::HTTP_NOT_FOUND);
            }

            return new JobOfferResource($jobOffer);
        } catch (\Exception $e) {
            return $this->errorResponse('Error retrieving job offer', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/job-offers",
     *     summary="Create a new job offer",
     *     tags={"jobOffers"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="title", type="string", example="Job Title"),
     *             @OA\Property(property="description", type="string", example="Job Description"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Job offer created successfully",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function store(CreateJobOfferRequest $request)
    {
        try {
            // Retrieve the validated data from the request
            $data = $request->validated();
            Log::info('JobOfferController@store: Request Data', ['data' => $data]);
            $jobOffer = $this->jobOfferRepository->create($data);
    
            return new JobOfferResource($jobOffer);
        } catch (\Exception $e) {
            Log::error('JobOfferController@store: Exception', ['exception' => $e->getMessage()]);

            return $this->errorResponse('Error creating job offer', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * @OA\Put(
     *     path="/api/job-offers/{id}",
     *     summary="Update a job offer",
     *     tags={"jobOffers"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the job offer",
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="title", type="string", example="Job Title"),
     *             @OA\Property(property="description", type="string", example="Job Description"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Job offer updated successfully",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Job offer not found",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function update(UpdateJobOfferRequest $request, $id)
    {
        try {
            $jobOffer = $this->jobOfferRepository->find($id);

            if (!$jobOffer) {
                return $this->errorResponse('Job offer not found', Response::HTTP_NOT_FOUND);
            }

            $data = $request->validated();
            $jobOffer = $this->jobOfferRepository->update($jobOffer, $data);

            return new JobOfferResource($jobOffer);
        } catch (\Exception $e) {
            return $this->errorResponse('Error updating job offer', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/job-offers/{id}",
     *     summary="Delete a job offer",
     *     tags={"jobOffers"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the job offer",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Job offer deleted successfully"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Job offer not found",
     *     ),
     *     security={{ "sanctum": {} }}
     * )
     */
    public function destroy($id)
    {
        try {
            $jobOffer = $this->jobOfferRepository->find($id);

            if (!$jobOffer) {
                return $this->errorResponse('Job offer not found', Response::HTTP_NOT_FOUND);
            }

            $this->jobOfferRepository->delete($jobOffer);

            return response()->json(null, Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return $this->errorResponse('Error deleting job offer', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

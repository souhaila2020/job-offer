<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ResumeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'formation' => $this->formation,
            'experience' => $this->experience,
            'competence' => $this->competence,
            'pdf_path' => $this->pdf_path,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}


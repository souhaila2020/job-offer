<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    use HasFactory;
    protected $table = 'resumes';

    protected $fillable = [
        'formation',
        'experience',
        'competence',
        'pdf_path',
    ];
    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }
}

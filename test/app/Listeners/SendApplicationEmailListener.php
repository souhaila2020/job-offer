<?php
namespace App\Listeners;

use App\Events\JobApplicationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class SendApplicationEmailListener implements ShouldQueue
{
    public function handle(JobApplicationEvent $event)
    {
        Log::info('hi:');
        // $user = $event->user;
        $jobOffer = $event->jobOffer;
        // $authEmail = $event->authEmail;
        // $resumePath = $event->resumePath;

        Mail::send('emails.application', ['jobOffer' => $jobOffer], function ($message) use ($jobOffer) {
            $message->to('sakly.souhaila.93@gmail.com')
                ->subject('New Job Application - ' . $jobOffer->title)
                ->from('sakly.souhailaaa@gmail.com');
                // ->attach(public_path($resumePath), ['as' => 'user_cv.pdf']);
        });
    }
}




<?php

namespace App\Events;

use App\Models\JobOffer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class JobApplicationEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    // public $user; // Unused for now, potential future use
    public $jobOffer;
    // public $authEmail; // Unused for now, potential future use
    // public $resumePath; // Descriptive name

    public function __construct(JobOffer $jobOffer)
    {
        // $this->user = $user;
        $this->jobOffer = $jobOffer;
        // $this->authEmail = $authEmail;
        // $this->resumePath = $resumePath;
        Log::info('Email sent for job application:', ['job_offer_id' => $jobOffer->id]);
    }
}

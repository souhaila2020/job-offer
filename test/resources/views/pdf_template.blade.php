<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resume</title>
</head>
<body>
    <h1>Resume</h1>

    <h2>{{ $data['formation'] }}</h2>
    <p>{{ $data['experience'] }}</p>
    <p>{{ $data['competence'] }}</p>

</body>
</html>
